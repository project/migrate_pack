# Migrate Pack

The goal of this project is to simply and streamline the Drupal 7
to 8 migration process. Migrate Pack provides Composer dependencies,
patches along with configuration and plugins to assist with migration.

The overall approach is to provide default settings that drive which
migrations, fields, bundles and other configuration are skipped in an
effort to minimize errors that otherwise halt migration. All settings
can be overridden by overriding the migrate_pack.settings.yml file in
your project configuration folder.

## Configuration
By default all migration config provided with this module will assume
the "migrate_drupal_7" migration group.

Be sure to run the `drush migrate:upgrade` command prior to viewing the
Migration config or you will see an error.

## Usage
- Run Composer require to pull down the migration feature:
- `$ composer require drupal/migrate_pack`


- Enable the migration feature and it's dependencies:
- `$ drush en -y migrate_pack`


- Run migrate commands:
- `$ drush migrate:upgrade` command (use --help for parameters)
- `$ drush migrate:import --all` to run all migrations


- Export your configuration to your sync directory:
- `$ drush cex -y`


- Modify configuration as needed. It's recommended to remove or skip
configuration
- Migrate yml files once you have it captured in the project sync directory.
At that point you will likely only want to run Content migrations.

## Migrations included
- Aliases
- Comments
- Content types
- Field collections to Paragraphs (with patch)
- Fields (D8 Core)
- Files
- Nodes
- Menus
- Menu Items
- Metatags
- Pathauto configuration
- Redirects
- Taxonomy terms
- Users
- Vocabularies
- Webforms (7.x-4.x only)

## Migrations currently not included
- Image styles
- System settings
- Theme settings
- User roles and permissions

## Roadmap
- Provide Admin UI to manage which migration items are skipped.
- Drupal 9 support.
- Media migration.

### Maintainers

* George Anderson (geoanders) - https://www.drupal.org/u/geoanders
